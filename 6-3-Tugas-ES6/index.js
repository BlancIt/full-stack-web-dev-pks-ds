// Soal 1

console.log("\nJawaban Soal 1\n");

const kelilingPersegiPanjang = (panjang, lebar) => {
    return 2 * (panjang + lebar);
} 

const luasPersegiPanjang = (panjang, lebar) => {
    return panjang * lebar;
} 

console.log(`Kelling persegi panjang dengan panjang 8 dan lebar 5 adalah ${kelilingPersegiPanjang(8, 5)}, sedangkan luasnya adalah ${luasPersegiPanjang(8, 5)}.`);

 // Soal 2
console.log("\nJawaban Soal 2\n")

const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: `${firstName} ${lastName}`
  }
}
   
// Driver Code 
console.log(newFunction("William", "Imoh").fullName)

// Soal 3
console.log("\nJawaban Soal 3\n");

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject

console.log(firstName, lastName, address, hobby)

// Soal 4
console.log("\nJawaban Soal 4\n");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

//Driver Code
console.log(combined)

// Soal 5
console.log("\nJawaban Soal 5\n");

const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
var after = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`
console.log(`Before: ${before}`)
console.log(`\nAfter: ${after}`)