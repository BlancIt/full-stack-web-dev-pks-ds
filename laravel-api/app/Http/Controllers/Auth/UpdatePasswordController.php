<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\OtpCode;
use Carbon\Carbon;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        //set validation
        $validator = Validator::make($allRequest, [
            'email' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User Not Found'
            ], 400);
        }

        $user->update([
            'password' => Hash::make($request->password)
        ]);


        return response()->json([
            'success' => true,
            'message' => 'Password Changed',
            'data'    => $user
        ]);

    }
}
