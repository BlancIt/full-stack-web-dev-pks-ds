<?php

namespace App\Listeners;

use App\Events\OtpCodeStoredForRegenerationEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\RegenerationMail;
use Illuminate\Support\Facades\Mail;
use App\OtpCode;

class SendEmailForRegeneration implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpCodeStoredForRegenerationEvent  $event
     * @return void
     */
    public function handle(OtpCodeStoredForRegenerationEvent $event)
    {
        Mail::to($event->otp_code->user->email)->send(new RegenerationMail($event->otp_code));
    }
}
