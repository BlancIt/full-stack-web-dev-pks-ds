<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <a>Hello {{ $comment->user->name }}! Your comment "{{ $comment->content }}" has successfully been submitted! The author, {{ $comment->post->user->name }}, can now see your comment.</a>
</body>
</html>