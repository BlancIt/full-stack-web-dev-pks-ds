<?php

namespace App\Listeners;

use App\Events\OtpCodeStoredForRegistrationEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\RegistrationMail;
use Illuminate\Support\Facades\Mail;
use App\OtpCode;

class SendEmailForRegistration implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpCodeStoredForRegistrationEvent  $event
     * @return void
     */
    public function handle(OtpCodeStoredForRegistrationEvent $event)
    {
        Mail::to($event->otp_code->user->email)->send(new RegistrationMail($event->otp_code));
    }
}
