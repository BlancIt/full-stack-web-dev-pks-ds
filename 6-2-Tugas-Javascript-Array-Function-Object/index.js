// Soal 1

console.log("\nJawaban Soal 1\n");

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var daftarHewanSorted = daftarHewan.sort();

daftarHewanSorted.forEach(function(item){
    console.log(item);
 });

 // Soal 2
 console.log("\nJawaban Soal 2\n")

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" };

function introduce(data) {
    return "Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby;
}

var perkenalan = introduce(data);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

// Soal 3
console.log("\nJawaban Soal 3\n");

function hitung_huruf_vokal(word) {
    var result = 0;
    var splittedWord = word.split('');
    splittedWord.forEach(function(item){
        if (item.match(/[aeiou]/gi)) {
            result += 1;
        }
    });
    return result;
    //return (word.match(/[aeiou]/gi) || []).length;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

// Soal 4
console.log("\nJawaban Soal 4\n");

function hitung(angka) {
    return (angka * 2) - 2;
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8